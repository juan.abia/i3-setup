#!/usr/bin/bash

# Summary by ChatGPT:
# This script installs and configures a number of packages, flatpacks, and configuration files 
# for the i3 window manager. The script checks to ensure that it is being run from the root 
# folder of the repository and that it is being run with root privileges. It then installs packages, 
# installs flatpaks, links configuration files, sets up i3-resurrect, installs and configures i3blocks, 
# and installs Fontawesome. Finally, the script sets up a lockscreen background and sets up git aliases.

set -e

function greenprint {
    echo -e "\033[1;32m${1}\033[0m"
}

# Test that we are running the script from the root folder of the repo
if [[ $(pwd | xargs basename) != "i3-setup" ]];then
	echo "This is script is intended to run from the root folder of the repo"
	exit 1
fi

# Test that the script is being run as root user
if [ "$(id -u)" != "0" ]; then
	echo "This script must be run as root" 1>&2
	exit 1
fi

user=$(logname)
as_user="sudo -u $user"

# Install packages
greenprint "Installing packages"
dnf install -y i3 i3lock xbacklight feh conky i3blocks keychain dunst compton \
	volumeicon rofi light acpi sysstat pavucontrol flatpak arandr python3-pip \
	xdotool bolt xev flameshot blueberry

# Installing vscode
rpm --import https://packages.microsoft.com/keys/microsoft.asc
sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
dnf update
dnf install -y code

# Install flatpaks
greenprint "Installing flatpacks"
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install -y flathub com.google.Chrome \
			 org.ferdium.Ferdium \
			 org.contourterminal.Contour \
			 com.spotify.Client

flatpak_bins=/var/lib/flatpak/exports/bin
[ -f $flatpak_bins/com.google.Chrome ] && mv $flatpak_bins/com.google.Chrome $flatpak_bins/google-chrome
[ -f $flatpak_bins/org.ferdium.Ferdium ] && mv $flatpak_bins/org.ferdium.Ferdium $flatpak_bins/ferdium
[ -f $flatpak_bins/org.contourterminal.Contour ] && mv $flatpak_bins/org.contourterminal.Contour $flatpak_bins/contour
[ -f $flatpak_bins/com.spotify.Client ] && mv $flatpak_bins/com.spotify.Client $flatpak_bins/spotify

# Hard link ~/.i3/config to repo config
greenprint "Setting up config files"
mkdir -p /home/${user}/.i3/
ln -f $PWD/src/i3_config /home/${user}/.i3/config
echo " - Created hard link of i3 config"

# Remove SDG directory scheme
rm -rf ~/.config/i3/
echo " - Removed i3 SDG directory scheme"

# Setup dunst
mkdir -p /home/${user}/.config/dunst
rm -f /home/${user}/.config/dunst/dunstrc
ln -f $PWD/src/dunst/dunstrc /home/${user}/.config/dunst/dunstrc

# Setup i3 resurrect
$as_user pip3 install -q --user --upgrade i3-resurrect
rm -drf /home/${user}/.i3/i3-resurrect
ln -sf $PWD/src/i3-resurrect /home/${user}/.i3/i3-resurrect
ln -f $PWD/src/scripts/i3restore /home/${user}/.local/bin/i3restore
echo " - Created links of i3-resurrect"

# Setup setmonitors
ln -f $PWD/src/scripts/setmonitors /home/${user}/.local/bin/setmonitors
mkdir -p /home/${user}/.screenlayout
ln -f $PWD/src/scripts/screenlayout/1_screen.sh /home/${user}/.screenlayout/1_screen.sh
ln -f $PWD/src/scripts/screenlayout/2_screen.sh /home/${user}/.screenlayout/2_screen.sh
echo " - Created hard link of setmonitors"

# Setup power menu (rofi_power)
ln -f $PWD/src/scripts/rofi_power /home/${user}/.local/bin/rofi_power
echo " - Created hard link of rofi_power"

# Hard link volumeicon config
rm -f /home/${user}/.config/volumeicon/volumeicon
ln -f $PWD/src/volumeicon/config /home/${user}/.config/volumeicon/volumeicon
mkdir -p /usr/share/volumeicon/icons/jabia/
cp -u $PWD/src/volumeicon/icons/* /usr/share/volumeicon/icons/jabia/
echo " - Created hard link of volumeicon"

# Hard link bashrc 
ln -f $PWD/src/bashrc /home/${user}/.bashrc
echo " - Created hard link of bashrc"

# Hard link gtk config file
rm -f /home/${user}/.config/gtk-3.0/settings.ini
ln -f $PWD/src/gtk_config /home/${user}/.config/gtk-3.0/settings.ini
echo " - Created hard link of gtk config"

# Setup i3blocks
mkdir -p /home/${user}/.i3/i3blocks
ln -f $PWD/src/i3blocks/config /home/${user}/.i3/i3blocks/config
rm -f /etc/i3blocks.conf
ln -f $PWD/src/i3blocks/config /home/${user}/.i3blocks.conf
mkdir -p /home/${user}/.i3/i3blocks/i3blocks-modules
ln -f $PWD/src/i3blocks/i3blocks-modules/* /home/${user}/.i3/i3blocks/i3blocks-modules/
echo " - Created hard links of i3blocks"

# Setup rofi
mkdir -p /home/${user}/.config/rofi
rm /home/${user}/.config/rofi/config.rasi
echo "@theme \"$PWD/src/rofi/rounded-orange-dark.rasi\"" > /home/${user}/.config/rofi/config.rasi

# Get fontawesome
greenprint "Installing fonts"
wget --no-verbose https://use.fontawesome.com/releases/v6.0.0/fontawesome-free-6.0.0-web.zip -O /tmp/font_awesome.zip 
unzip -o /tmp/font_awesome.zip -d /tmp/font_awesome >/dev/null

mkdir -p /home/${user}/.fonts/
cp /tmp/font_awesome/fontawesome-free-6.0.0-web/webfonts/*.ttf /home/${user}/.fonts/

# Setup background
greenprint "Setting up background"
cp $PWD/src/bg_lock.png /home/${user}/.i3/bg_lock.png

# Add git aliases (asuming 1000 is the main user id)
greenprint "Setting up git aliases"
config_base="$as_user git config --global"
$config_base alias.fix 'commit --amend --no-edit'
$config_base alias.st 'status'
$config_base alias.ch 'checkout'
$config_base alias.ll 'log --oneline'
$config_base alias.last 'log 1 HEAD --stat'
$config_base alias.ri-origin 'rebase --interactive origin/main'
$config_base alias.ri-upstream 'rebase --interactive upstream/main'
$config_base push.autoSetupRemote true

# Reload i3 config
greenprint "Reloading i3 config"
i3-msg restart
