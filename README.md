# i3-setup

The aim of this repo is to have an easy way to configure a new computer with the exact setup I had on my previous machine.

# How to install
Note: keep in mind that this setup is thought to be updated everytime there's a change, so parts of the installation rely on files inside the repo (I should probably change that). So do not remove the repo once installed or some thing might fail.
```
git clone https://gitlab.com/juan.abia/i3-setup.git
cd i3-setup.git
sudo bash setup.sh
```
